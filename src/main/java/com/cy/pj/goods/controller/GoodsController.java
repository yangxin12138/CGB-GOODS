package com.cy.pj.goods.controller;

import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.websocket.server.PathParam;
import java.util.List;

@Controller //@Sercice ,@ Compoent
@RequestMapping("/goods/")
public class GoodsController {
    //has a + di
    @Autowired
    private GoodsService goodsService;

    @RequestMapping("doGoodsUI")
    public String doGoodsUI(Model model){
        //调用业务层方法获取商品信息
        List<Goods> list= goodsService.findGoods();
        //将数据存储到请求作用域
        model.addAttribute("list",list);
        return "goods";
    }

    /**
     * {id} Restful 编码风格 {}中是一个变量表达式
     * 可以在方法参数中使用@PathVariable注解对参数进行描述
     * @param id
     * @return
     */
    @RequestMapping("doDeleteById/{id}")
    public String doDeleteById(@PathVariable Integer id,Model model){
        System.out.println("id="+ id);
        goodsService.deleteById(id);
        List<Goods> list=goodsService.findGoods();
        model.addAttribute("list",list);
        return "redirect:/goods/doGoodsUI";
    }

    @RequestMapping("doSaveGoods")
    public String doSaveGoods(Goods entity){
        goodsService.saveGoods(entity);
        return "redirect:/goods/doGoodsUI";
    }
    @RequestMapping("doGoodsAddUI")
    public String doGoodsAddUI(){
        return "goods-add";
    }

    @RequestMapping("doFindById/{id}")
    public String doFindById(@PathVariable Integer id,Model model){
        Goods goods=goodsService.findById(id);
        model.addAttribute("goods",goods);
        return "goods-update";
    }
    @RequestMapping("doUpdateGoods")
    public String doUpdateGoods(Goods goods){
        goodsService.updateGoods(goods);
        return "redirect:/goods/doGoodsUI";
    }
}
