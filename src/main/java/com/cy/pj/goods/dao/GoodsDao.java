package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface GoodsDao {
    @Select("select * from tb_goods")
    List<Goods> findGoods();

    @Delete("delete from tb_goods where id=#{id}")
    int deleteById(Integer id);

    @Insert("Insert into tb_goods(name,remark,createdTime) values(#{name},#{remark},now())")
    int insertObject(Goods entity);

    @Select("select * from tb_goods where id=#{id}")
    Goods findById(Integer id);

    @Update("update tb_goods set name=#{name},remark=#{remark} where id=#{id}")
    int updateGoods(Goods goods);
}
