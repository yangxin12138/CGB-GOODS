package com.cy.pj.goods.service;

import com.cy.pj.goods.dao.GoodsDao;
import com.cy.pj.goods.pojo.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements  GoodsService{
    @Autowired
    private GoodsDao goodsDao;

    @Override
    public List<Goods> findGoods() {
        return goodsDao.findGoods();
    }

    @Override
    public int deleteById(Integer id) {
        long t1=System.currentTimeMillis();
        int rows=goodsDao.deleteById(id);
        long t2=System.currentTimeMillis();
        System.out.println("execute Time:" +(t2-t1));
        return rows;
    }

    @Override
    public int saveGoods(Goods entity) {
        int rows=goodsDao.insertObject(entity);
        return rows;
    }

    @Override//更新查询方法
    public Goods findById(Integer id) {
        //0.0
        return goodsDao.findById(id);
    }

    @Override//返回一个goods对象
    public int updateGoods(Goods goods) {
        return goodsDao.updateGoods(goods);
    }


}
